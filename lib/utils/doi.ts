const doiLinkRegex = /^https?:\/\/(((dx.)?doi.org)|doai.io)\/(10\.\d{4,}\/.*)$/;

export function isDoiLink(link: string) {
  return doiLinkRegex.test(link);
}

export function convertToDoiLink(doi: string) {
  return 'https://doai.io/' + doi;
}

export function convertToDoi(doiLink: string) {
  // Links to ORCID profiles are of the form  `https://orcid.org/<orcid>`
  const matches = doiLink.match(doiLinkRegex);

  return (matches === null) ? null : matches[4];
}
