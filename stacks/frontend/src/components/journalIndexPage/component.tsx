import './styles.scss';

import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { InitialisedPeriodical } from '../../../../../lib/interfaces/Periodical';
import { getPeriodicals } from '../../services/periodical';
import { JournalIndex } from '../journalIndex/component';
import { Spinner } from '../spinner/component';

interface JournalIndexPageState {
  periodicals?: null | InitialisedPeriodical[];
}

export class JournalIndexPage
  extends React.Component<
  RouteComponentProps<{}>,
  JournalIndexPageState
> {
  public state: JournalIndexPageState = {};

  constructor(props: any) {
    super(props);
  }

  public async componentDidMount() {
    try {
      const periodicals = await getPeriodicals();
      this.setState({
        periodicals,
      });
    } catch (e) {
      this.setState({ periodicals: null });
    }
  }

  public render() {
    if (typeof this.state.periodicals === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.periodicals === null) {
      return (
        <div className="callout alert">
          The list of journals could not be loaded, please refresh this page to try again.
        </div>
      );
    }

    return (
      <JournalIndex
        periodicals={this.state.periodicals}
        url={this.props.match.url}
      />
    );
  }
}
